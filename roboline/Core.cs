﻿using System;
using Microsoft.Xna.Framework;

namespace roboline
{

	public class MotorsPower
	{
		public int left, right;
		public MotorsPower(int left, int right)
		{
			this.left = left;
			this.right = right;
		}
	}

	public class RoboModel
	{
		public static float Cross2d(Vector2 a, Vector2 b)
		{
			return a.X * b.Y - b.X * a.Y;
		}
		//TODO: public static float SCALE = 0.01f; //1px = 0.01m
		// Body
		private float mass;
		private float inertia;
		public float angle;
		private float radius;
		private float WheelInertia;
		// circuit parameters
		private float CircKm;
		private float friction;
		private float Kv;
		private float CircL;
		private float CircR;
		private Vector2 omega;
		private Vector2 icurr;
		private float scalingPar;
		private Vector2 pos;
		private Vector2 centerPos;

		//physics solution options
		private float dt;
		private int qualityN;
		private float step;
		private float[] Time;
		private Vector2 U;
		public float[] angleA;
		public Vector2[] centerA;
		float[] s;

		public Vector2 position { get { return pos; } }
		public Vector2 center { get { return centerPos; } }
		public float rotation { get { return angle + (float)Math.PI / 2; } }

		// Robot
		private int sensorsN;
		private float[] sensorAngl;
		private Vector2[] sensorPos;
		private Vector2[] motorsPos;
		public MotorsPower power;
		private int[] sensorState;
		private Vector2[] Omega;
		private Vector2[] Curr;

		public float velocity;

		public Vector2[] sensors_position { get { return sensorPos; } }

		public RoboModel(Vector2 position, float rotation)
		{
			//TODO: calculate REAL scaled numbers for REAL model from these links:
			mass = 0.4f;
			radius = 4f;
			inertia = 850f;
			CircKm = 1000f;
			WheelInertia = 1000f;
			friction = 250f;
			Kv = 1f;
			CircL = 0.01f;
			CircR = 1f;

			//????
			scalingPar = 10f;

			pos = position;
			angle = rotation;
			centerPos = new Vector2(91, 89);
			power = new MotorsPower(0, 0);
			omega = new Vector2(0, 0);
			icurr = new Vector2(0, 0);

			UpdSensorsPos(angle); //starting positions for sensors
			
			//Do we need it?
			motorsPos = new Vector2[2];
			motorsPos[0] = new Vector2(0, -10);
			motorsPos[1] = new Vector2(0, 10);

			sensorState = new int[sensorsN];
			for (int i = 0; i < sensorState.Length; i++)
			{
				sensorState[i] = 0;
			}
		}

		public int[] sensorsData(Color[] colors)
		{
            sensorState = new int[5];
            for (int i = 0; i < 5; i++)
            {
                if (colors[i].A == 0)
				{
					sensorState[i] = 0;
				}
                else
                {
                    sensorState[i] = 1;
                }
            }
                return sensorState;
		}

		private void UpdSensorsPos(float angle)
		{
			sensorsN = 5;
			sensorPos = new Vector2[sensorsN];
			sensorAngl = new float[sensorsN];

			sensorAngl[0] = (float) -0.73; 
		    sensorAngl[1] = (float) -0.407;
			sensorAngl[2] = (float) -0.055;
			sensorAngl[3] = (float)0.322;
			sensorAngl[4] = (float)0.597;

			for(int i = 0; i <= 4; i++)	
			{
                sensorPos[i].X = ((float)Math.Cos(sensorAngl[i] + angle) * 93 + position.X);
                sensorPos[i].Y = ((float)Math.Sin(sensorAngl[i] + angle) * 93 + position.Y);
			}
		 
		}
        
		public void setMotors(MotorsPower power)
		{
			this.power = power;
		}

		public void update(float deltaT)
		{
			//TODO: more accurate calculations method
			dt =  deltaT;
			DSolve(dt, 100);
			angle = calcAngle(dt, 100);
            UpdSensorsPos(angle);
			pos = calcPos(dt, 100);
		}

		//REAL physics here 8]
		//TODO: add momentum of static friction!!
		public bool DSolve(float dt, int qualityN)
		{
			this.qualityN = qualityN;
			Vector2[] OmegaHelp;
			Vector2[] CurrHelp;
			OmegaHelp = new Vector2[qualityN];
			CurrHelp = new Vector2[qualityN];
			step = dt / ((float)qualityN);
			Omega = new Vector2[qualityN];
			Curr = new Vector2[qualityN];
			Time = new float[qualityN];
			//scalingPar??
			U = new Vector2();
			U.X = scalingPar * power.left;
			U.Y = scalingPar * power.right;

			Time[0] = 0;
			Omega[0] = omega;
			Curr[0] = icurr;
			OmegaHelp[0] = omega;
			CurrHelp[0] = icurr;
			for (int i = 1; i < qualityN; i++)
			{
				Time[i] = Time[i - 1] + step;
				OmegaHelp[i] = Omega[i - 1] + step * rightPart(Omega[i - 1], Curr[i - 1], Time[i - 1], 0);
				CurrHelp[i] = Curr[i - 1] + step * rightPart(Omega[i - 1], Curr[i - 1], Time[i - 1], 1);
				Omega[i] = Omega[i - 1] + step / 2 * (rightPart(Omega[i - 1], Curr[i - 1], Time[i - 1], 0) + rightPart(OmegaHelp[i], CurrHelp[i], Time[i], 0));
				Curr[i] = Curr[i - 1] + step / 2 * (rightPart(Omega[i - 1], Curr[i - 1], Time[i - 1], 1) + rightPart(OmegaHelp[i], CurrHelp[i], Time[i], 1));
			}
			omega = Omega[qualityN - 1];
			icurr = Curr[qualityN - 1];
			return true;
		}

		public Vector2 rightPart(Vector2 omegaF, Vector2 currF, float timeF, int OmegaOrCurr) //how stupid I am that I write code like this?
		{
			Vector2 a, b, c, d;
			float a1, a0;
			Vector2 function;

			if (OmegaOrCurr == 0)
			{
				a0 = 1 / (2 * (inertia + mass * radius * radius) * (inertia + WheelInertia));
				a = new Vector2(CircKm*(2*inertia + mass*radius*radius + WheelInertia), CircKm*(WheelInertia - mass*radius*radius));
				b = new Vector2(-friction * (2*inertia + mass * radius * radius + WheelInertia), -friction * (WheelInertia - mass * radius * radius));
				c = new Vector2(CircKm * (WheelInertia - mass * radius * radius), CircKm * (2 * inertia + mass * radius * radius + WheelInertia));
				d = new Vector2(-friction * (WheelInertia - mass * radius * radius), - friction*(2 * inertia + mass * radius * radius + WheelInertia));
				function.X = a0 * Vector2.Dot(a, currF) + a0 * Vector2.Dot(b, omegaF);
				function.Y = a0 * Vector2.Dot(c, currF) + a0 * Vector2.Dot(d, omegaF);
				return function;
			}
			else
			{
				a1 = 1 / CircL;
				a = new Vector2(CircR, 0);
				b = new Vector2(Kv, 0);
				c = new Vector2(0, CircR);
				d = new Vector2(0, Kv);
				function.X = - a1 * Vector2.Dot(a, currF) - a1 * Vector2.Dot(b, omegaF) + a1*U.X;
				function.Y = - a1 * Vector2.Dot(c, currF) - a1 * Vector2.Dot(d, omegaF) + a1*U.Y;
				return function;
			}
		}

		public float calcAngle(float dt, int qualityN) // takes array and returns angle
		{
			//integration to get angle of robot
			s = new float[qualityN];
			angleA = new float[qualityN + 1];
			angleA[0] = angle;
			step = dt / qualityN;
			for (int i = 0; i < qualityN; i++)
			{
				if ((i == 0) || (i == qualityN - 1))
				{
					s[i] = 1 / 2;
				}
				else
				{
					s[i] = 1;
				}
				angleA[i + 1] = angleA[i] + step * (this.Omega[i].X - this.Omega[i].Y) / 2 * s[i] / inertia;
			}
			return angleA[qualityN];
		}

		public Vector2 calcPos(float dt, int qualityN) //  returns center position 
		{
			s = new float[qualityN];
			centerA = new Vector2[qualityN + 1];
			centerA[0] = pos;
			step = dt / qualityN;
			for (int i = 0; i < qualityN; i++)
			{
				if ((i == 0) || (i == qualityN - 1))
				{
					s[i] = 1 / 2;
				}
				else
				{
					s[i] = 1;
				}
				centerA[i + 1].X = centerA[i].X + step * (this.Omega[i].X + this.Omega[i].Y) * ((float)Math.Cos(angleA[i])) / 2 * s[i] / mass /radius / radius; /// added parameters to make more realistic movements
				centerA[i + 1].Y = centerA[i].Y + step * (this.Omega[i].X + this.Omega[i].Y) * ((float)Math.Sin(angleA[i])) / 2 * s[i] / mass / radius / radius;
			}
			velocity = this.Omega[qualityN-1].X + this.Omega[qualityN-1].Y;
			return centerA[qualityN];
		}

	} // END of RoboModel
}