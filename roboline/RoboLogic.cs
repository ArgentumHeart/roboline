﻿using System;
#region Using Statements


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;

#endregion

namespace roboline
{
	public class RoboLogic
	{
		private float totalT = 0f;
		private int[] motors;
		private float frequency;
		private float lastT = 0f;
		private float error;
		private float interror;
		private float diferror;
		private float preverror;
		public int command;
		private int[] motorsSign = new int[2];
		public PID OurPID;
		public RoboLogic()
		{
			error = 0f;
			preverror = 0f;
			motors = new int[2];
			error = 0f;
			diferror = 0f;
			interror = 0f;
			motors[0] = 0;
			motors[1] = 0;
			OurPID = new PID();
		}

		
						
		public MotorsPower platform(int[] sensors, float dt, KeyboardState state)
		{
			int turnVal = 20;
			int scaling = 40; //SCALING IS IMPORTANT MUST DEPEND ON SPEED AND INTEGRAL

			totalT += dt;
			motorsSign[0] = 0;
			motorsSign[1] = 0;
			int motorsDef = 200;
			bool keyNotUsed = true;
			bool manual = false;

			if (state.IsKeyDown(Keys.Left))
			{
				motorsSign[0] -= turnVal;
				motorsSign[1] += turnVal;
				keyNotUsed = false;
			}
			if (state.IsKeyDown(Keys.Right))
			{
				motorsSign[0] += turnVal;
				motorsSign[1] -= turnVal;
				keyNotUsed = false;
			}
			if (state.IsKeyDown(Keys.Up))
			{
				if (motorsSign[0] != motorsSign[1])
				{
					if (motorsSign[0] < motorsSign[1])
					{
						motorsSign[0] = motorsSign[1];
					}
					else
					{ motorsSign[1] = motorsSign[0]; }
				}
				else
				{
					motorsSign[0] += turnVal;
					motorsSign[1] += turnVal;
				}
				keyNotUsed = false;
			}
			if (state.IsKeyDown(Keys.Down))
			{
				if (motorsSign[0] != motorsSign[1])
				{
					if (motorsSign[0] < motorsSign[1])
					{
						motorsSign[0] = motorsSign[1];
					}
					else
					{ motorsSign[1] = motorsSign[0]; }
				}
				else
				{
					if ((motorsSign[0] < 0) || (motorsSign[1] < 0))
					{
						motorsSign[0] -= turnVal;
						motorsSign[1] -= turnVal;
					}
					else
					{
						motorsSign[0] -= 4 * turnVal;
						motorsSign[1] -= 4 * turnVal;
					}
				}
				keyNotUsed = false;
			}
			if (state.IsKeyDown(Keys.Space))
			{
				manual = !manual;
			}

			if (keyNotUsed && !manual)
			{
				if (totalT >=   dt)
				{

					OurPID.SetTunings(1f, 0.0005f, -500f);//WORKING NOW : 10, 0.005, -400
					command = OurPID.Calc(sensors, dt, scaling);

					motorsSign[1] = command;
					motorsSign[0] = -(int)(command );
					totalT -=  dt;
					motors[0] = motorsDef + motorsSign[0];
					motors[1] = motorsDef + motorsSign[1];
					return new MotorsPower(motors[0], motors[1]); 
				};
			};


			totalT += dt;
			motors[0] += motorsSign[0];
			motors[1] += motorsSign[1];
			return new MotorsPower(motors[0],motors[1]); 
		}

	}
}

