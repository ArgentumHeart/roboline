﻿using System;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;


namespace roboline
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		private float zoom = 0.45f;
		private Texture2D robot;
        //Microsoft.Xna.Framework.Graphics.SpriteFont font;
        Vector2 textSize;
		private Texture2D map;

        private Texture2D pew04; //red square
        private Vector2 pew0, pew4; // red squares cooordinates
        private Texture2D detector; //detector image
        public Rectangle[] detPos;
        public Color detCol;
        public int width;
        private SpriteFont defaultFont;

		private RoboModel robotModel;
		private RoboLogic logic;
		//for color detection
		public Color[] sensorColors = new Color[5];
		public Color[] mapcopy;
		Vector2[] sensPos = new Vector2[5];

		public Game1 ()
		{
			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";	            
			graphics.IsFullScreen = false;
			//graphics.PreferMultiSampling = true; // antialising
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize ()
		{
			// TODO: Add your initialization logic here
			robotModel = new RoboModel(new Vector2(200,200), 0f);
			logic = new RoboLogic ();
			base.Initialize ();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent ()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch (GraphicsDevice);

			robot = Content.Load<Texture2D> ("robo");

			//TODO: use this.Content to load your game content here 
			robot = Content.Load<Texture2D> ("robo");
			map = Content.Load<Texture2D> ("map2");
            pew04 = Content.Load<Texture2D> ("rectangle");
            detector = Content.Load<Texture2D>("detector");
			defaultFont = Content.Load<SpriteFont> ("robofont");
            //Setting windows size
            graphics.PreferredBackBufferWidth = (int)Math.Ceiling(map.Width*zoom);
            graphics.PreferredBackBufferHeight = (int)Math.Ceiling(map.Height*zoom);
            graphics.ApplyChanges();
			//copying to get color data
			width = map.Width;
			int height = map.Height;
			mapcopy = new Color[height * width];
			map.GetData<Color>(mapcopy);
            detPos = new Rectangle[5];
            for(int i=0; i<5; i++)
            {
                detPos[i] = new Rectangle(70 + 110*i, 1492, 70, 70);
            }
            detCol = new Color();

		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update (GameTime gameTime)
		{
			// For Mobile devices, this logic will close the Game when the Back button is pressed
			if (GamePad.GetState (PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
			    Keyboard.GetState ().IsKeyDown (Keys.Escape)) {
				Exit ();
			}
			//Update logic here
			float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;		
			//Extracting information for sensors	
			int width = map.Width;
            int height = map.Height;
            int tempPos;
            for (int i = 0; i < 5; i++)
            {
                sensPos[i] = robotModel.sensors_position[i];
                if ((tempPos = (int)sensPos[i].Y * width + (int)sensPos[i].X) < 0)
                {
                    tempPos = 0;
                }
                else
                {
                    if (tempPos > width * height)
                    {
                        tempPos = width * height - 1;
                    }
                }
                sensorColors[i] = mapcopy[tempPos]; //sensors_positions is a relative
            }
            
            //TEST SHIT
            pew0 = new Vector2 (sensPos[0].X, sensPos[0].Y); // red squares coordinaters 
            pew4 = new Vector2 (sensPos[4].X, sensPos[4].Y); //
            //TEST SHIT ENDS

			//mapcopy.GetData<Color>(0, null, sensorColors, (int)sensPos[0].X * width + (int)sensPos[0].Y, 1); <- this should work, but doesn't 
			//updating robomodel

			KeyboardState state = Keyboard.GetState();
			MotorsPower power = logic.platform(robotModel.sensorsData(sensorColors), dt, state);
			robotModel.setMotors(power);
			robotModel.update(dt);

			base.Update (gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		/// 
		protected override void Draw (GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear (Color.White);
	
			//TODO: Add your drawing code here
			Matrix mzoom = Matrix.CreateScale (new Vector3 (zoom, zoom, 1));
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, transformMatrix: mzoom);
			spriteBatch.Draw(map, Vector2.Zero);
			spriteBatch.Draw(robot, robotModel.position, origin: robotModel.center, rotation: robotModel.rotation);



			spriteBatch.DrawString(defaultFont, "FPS: " + 1 / gameTime.ElapsedGameTime.TotalSeconds + " velocity = " + robotModel.velocity + " \n state of pixels: " + sensorColors[0].A + "  " + sensorColors[1].A + "  " + sensorColors[2].A + "  " + sensorColors[3].A + "  " + sensorColors[4].A, new Vector2(5, 5), Color.Black);
			//, 0f, Vector2.Zero, 2f, SpriteEffects.None, 0f);  //+ "     motors:        " + robotModel.power.left + "    "  +  robotModel.power.right
               
            for (int i = 0; i < 5; i++)
            {
                if (sensorColors[i].A == 0 )
                {
                    detCol = Color.White;
                }
                else
                {
                    detCol = Color.Black;
                }
                spriteBatch.Draw(detector, detPos[i], detPos[i], detCol, 0, Vector2.Zero, SpriteEffects.None, -1);
            }
			spriteBatch.Draw(map, Vector2.Zero);
			spriteBatch.End();
			
            base.Draw(gameTime);
           
		}
		
	}
}
