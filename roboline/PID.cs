﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace roboline
{
	public class PID 
	{
		int Input;
		int LastInput =0;
		float Err,LastErr=0;
		float ErrSum;
		float kp=0;
		float ki=0;
		float kd=0;
		float Output;

		public int Calc(int[] sensors, float dt, int scalingPar)
		{
			Input = 0;
			float timedomain = dt*60;
			for (int i = 0; i <= 4; i++ )
			{
				Input = Input - sensors[i] * (i - 2);				
			}
			if ((Input == 0) && (sensors[2] == 0)) Input = LastInput;
			Err = (float)Input;
			ErrSum += (Err * timedomain);
			Output = kp * (float)(Math.Pow(Math.Abs(Err), 1) * Math.Sign(Err)) + ki * ErrSum + kd *(LastErr - Err)/timedomain ;
			LastErr = Err;
			LastInput = Input;
			return (int)(Output*scalingPar);	
		}

			public void SetTunings(float Kp, float Ki, float Kd)
			{
				kp = Kp;
				ki = Ki;
				kd = Kd;
			}
	
	}

}
