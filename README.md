![Untitled-1.png](https://bitbucket.org/repo/kApGna/images/787379055-Untitled-1.png)
________________________________________________________________________________________________________________________________________________________________________

# Technical task and team information #

The main purpose of the project is educational. The core idea is to create a realistic model of line following robot that will adjust its settings to map specifications and reach the best possible performance for certain model. Project goals:

1. A realistic physical model of robot, visualized with Monogame.

2. Make it follow the line when found. At least badly :(

3. Adjusting movement with arrow keys. 

4. Visualize movement. 



### The list of project sections and responsible party: ###

* Robot model and physics   -  ![661571371-0-ArgentumHeart-avatar_avatar.png](https://bitbucket.org/repo/kApGna/images/2455896632-661571371-0-ArgentumHeart-avatar_avatar.png) Margarita D.
* Pid controller   -  ![user_blue.png](https://bitbucket.org/repo/kApGna/images/1843192113-user_blue.png)  Vadym S.
* Robot logic platform that implements communication between robot model and PID   -  ![user_blue.png](https://bitbucket.org/repo/kApGna/images/1843192113-user_blue.png)  Vadym S.
* In-game graphics and visualization aids at project [page](https://bitbucket.org/ArgentumHeart/roboline)   -  ![661571371-0-ArgentumHeart-avatar_avatar.png](https://bitbucket.org/repo/kApGna/images/2455896632-661571371-0-ArgentumHeart-avatar_avatar.png)  Margarita D.